package set4;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BinarySearch {

    private static String[] readFile(){
        String path = "set4/sample.csv";
        String line;
        List<String> data = new ArrayList<>();
        
        try{
            BufferedReader br = new BufferedReader(new FileReader(path));
            while((line = br.readLine()) != null){
                String[] words = line.split(",");
                data.addAll(Stream.of(words).map(word -> word).collect(Collectors.toList()));
            }
            br.close();
        }catch(FileNotFoundException ex){
            System.out.println("Specified File can not be found!");
            System.out.println("message: "+ex.getMessage());
        }catch(Exception ex){
            System.out.println(ex.getMessage());
        }

        return data.toArray(new String[data.size()]);
    }

    public static void search(String[] data, String keyword){
        Arrays.sort(data);
        int index = search(data, 0, data.length - 1, keyword);

        if(index < 0)
            System.out.println(keyword+" can not be found in the file.");
        else
            System.out.println(keyword+" found!");
    }

    private static int search(String[] data, int start, int end, String keyword) {
        if(end < start)
            return -1;
        
        int mid = (end + start)/2;
        int res = data[mid].compareToIgnoreCase(keyword);
        if(res == 0)
            return mid;
        
        if(res < 0)
            return search(data, mid+1, end, keyword);
        else
            return search(data, start, mid-1, keyword);
    }
    

    public static void main(String[] args) {
        String[] data = readFile();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a word to search it in the file");
        String keyword = sc.nextLine();
        sc.close();
        search(data, keyword);
    }
}