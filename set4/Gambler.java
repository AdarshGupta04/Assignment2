package set4;

import java.util.Scanner;

public class Gambler {
    
    public static void gamble(int sTake, int goal, int nTimes) {
        int won = 0;
        int temp = nTimes;
        
        while(nTimes != 0){
            boolean res = startGame(sTake, goal);
            if(res){
                sTake++;
                won++;
            }
            else
                sTake--;
            nTimes--;
        }
        
        int winPercentage = (won * 100) / temp;
        System.out.println("number of wins: "+won);
        System.out.println("percentage of win: "+winPercentage);
        System.out.println("percentage of loss: "+(100 - winPercentage));
    }

    private static boolean startGame(int sTake, int goal){
        if(Math.random() > 0.5)
            return true;
        return false;
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int sTake;
        int goal;
        int nTimes;

        try{
            sTake = Integer.parseInt(args[0]);
            goal = Integer.parseInt(args[1]);
            nTimes = Integer.parseInt(args[2]);
        }catch(Exception ex){
            System.out.println("Enter the $ you have");
            sTake = sc.nextInt();
            System.out.println("Enter the target $ you want to win.");
            goal = sc.nextInt();
            System.out.println("Enter the number of chances you wanna play");
            nTimes = sc.nextInt();
            sc.close();
        }

        gamble(sTake, goal, nTimes);
    }
}