package set4;

import java.util.HashSet;
import java.util.Set;

import list.Node;
import list.NodeStaticFeatures;

public class DetectLoop {

    private static boolean hasCycle(Node head){
        if(head == null || head.next == null)
            return false;
        
        Node slow = head;
        Node fast = head;

        while(fast.next != null && fast.next.next != null){
            fast = fast.next.next;
            slow = slow.next;
            if(fast == slow)
             return true;
        }
        return false;
    }

    private static Node removeCycle(Node head) {
        Node slow = head;
        Node fast = head;

        do{
            slow = slow.next;
            fast = fast.next.next;
        }while(slow != fast);

        Node entry = head;
        while(true){
            if(entry.next == slow.next){
                slow.next = null;
                break;
            }
            entry = entry.next;
            slow = slow.next;
        }
        return head;
    }
    
    public static void main(String[] args) {
        Node head = getloopedList();
        printList(head);
        boolean hasCycle = hasCycle(head);
        if(hasCycle){
            head = removeCycle(head);
            NodeStaticFeatures.printList(head);
        }
        else
            System.out.println("No cycle found in the list!");
    }

    private static Node getloopedList(){
        Node head = new Node(null, 1);
        Node node2 = new Node(null, 2);
        head.next = node2;
        Node node3 = new Node(null, 3);
        node2.next = node3;
        Node node4 = new Node(null, 4);
        node3.next = node4;
        Node node5 = new Node(node2, 5);
        node4.next = node5;

        return head;
    }

    private static void printList(Node head) {
        Set<Node> set = new HashSet<>();

        while(!set.contains(head)) {
            System.out.print(head.data+" -> ");
            set.add(head);
            head = head.next;
        }
        System.out.print(head.data+"");
        System.out.println();
    }
}