package set4;

import java.util.Scanner;

public class PowerOf2 {

    public static void printTable(int n) {
        if(n >= 31){
            System.out.println("Please enter a value less than 31");
            return;
        }
        int i = 1;
        while(i <= n){
            int num = (int) Math.pow(2, i);
            System.out.println(i+": "+num);
            i++;
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        try{
            n = Integer.parseInt(args[0]);
        }catch(Exception ex){
            System.out.println("Enter n");
            n = sc.nextInt();
            sc.close();
        }

        printTable(n);
    }
}