package list;

public class NodeStaticFeatures {

    public static Node createLL(){
        Node head = new Node(null, 10);
        int[] arr = {20, 30, 40, 50};
        Node tail = head;

        for(int i=0; i<arr.length; i++){
            Node newNode = new Node(null, arr[i]);
            tail.next = newNode;
            tail = newNode;
        }
        return head;
    }

    public static Node createCircularLL(){
        Node head = new Node(null, 10);
        int[] arr = {20, 30, 40, 50};
        Node tail = head;

        for(int i=0; i<arr.length; i++){
            Node newNode = new Node(null, arr[i]);
            tail.next = newNode;
            tail = newNode;
        }
        tail.next = head;

        return head;
    }

    public static void printList(Node head){
        while(head != null){
            System.out.print(head.data+" --> ");
            head = head.next;
        }
        System.out.println("End");
    }


    //Circular List Methods...
    public static int circularListLength(Node head){
        Node temp = head;
        int length = 0;

        do{
            length++;
            temp = temp.next;
        }while(temp != head);

        return length;
    }

    public static Node getCircularListTail(Node head) {
        Node temp = head;

        do{
            temp = temp.next;

        }while(temp.next != head);

        return temp;
    }

    public static void printCircularList(Node head){
        Node temp = head;
        do{
            System.out.print(temp.data+" --> ");
            temp = temp.next;
        }while(temp != head);

        System.out.println("head: "+temp.data);
    }
}