package list;

public class Node {
    public Node next;
    public Node prev;
    public int data;

    public Node(){

    }

    public Node(Node next, Node prev, int data){
        this.next = next;
        this.prev = prev;
        this.data = data;
    }

    public Node(Node next, int data){
        this.next = next;
        this.data = data;
    }

}