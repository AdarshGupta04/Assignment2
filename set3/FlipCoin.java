package set3;

import java.util.Scanner;

public class FlipCoin {

    public static void flipCoin(int flips){
        int tails = 0;
        for(int i=0; i<flips; i++){
            double random = Math.random();
            if(random < 0.5){
                tails++;
            }
        }
        int tailsPer = (tails * 100)/flips;
        System.out.println("Tails percentage: "+tailsPer);
        System.out.println("Heads percentage: "+(100 - tailsPer));
        
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number of times you want to flip the coin.");
        int flips = sc.nextInt();
        sc.close();
        
        flipCoin(flips);
    }
}