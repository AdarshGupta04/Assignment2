package set3;

import java.util.Scanner;

public class Coupon {
    
    private static int getCoupon(int n){
        return (int)(Math.random() * n);
    }

    public static int getTotalCount(int n){
        boolean[] isConsidered = new boolean[n];
        int count = 0;
        int distinct = 0;

        while(distinct < n){
            int value = getCoupon(n);
            count++;
            if(!isConsidered[value]){
                distinct++;
                isConsidered[value] = true;
            }
        }
        return count;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        sc.close();
        int count = getTotalCount(n);
        System.out.println("count: "+count);
    }
}