package set3;

import java.util.Scanner;

public class FindNumber {

    public static void guessTheNumber(int n, Scanner sc){
        int end = (int) Math.pow(2, n);
        System.out.println("Think of a number between 0 and "+(end - 1));
        if(n == 0){
            System.out.println("Your number is "+n);
            return;
        }
        findNumber(end, sc);
    }

    private static void findNumber(int end, Scanner sc){
        int start = 0;
        int res = -1;
        boolean userInput = false;
        boolean terminate = false;

        while(!terminate){
            int mid = (end + start)/2;
            System.out.println("Greater than or equal to "+(mid)+" ?");

            try{
                userInput = sc.nextBoolean();
            }catch(Exception ex){
                userInput = true;
                System.out.println("please enter a valid input that is either true or false");
                return;
            }

            if(end - start == 2){
                if(userInput)
                    res = mid;
                else
                    res = start;
                terminate = true;
                continue;
            }

            if(userInput)
                start = mid;
            else
                end = mid;
        }
        System.out.println("Your number is "+res);
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n;
        try{
            n = Integer.parseInt(args[0]);
        }catch(Exception ex){
            System.out.println("exception: "+ex.getClass());
            System.out.println("You have to enter a number through command line...");
            System.out.println("Enter the number now...");
            n = sc.nextInt();
        }

        guessTheNumber(n, sc);
        sc.close();
    }
    
}