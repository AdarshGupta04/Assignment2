package set3;

import java.util.Scanner;
import java.util.Arrays;

public class SearchingSorting {

    private static void sortAndSearch(String str, String keyword){
        str = str.toLowerCase();
        String[] string = str.split(" ");
        Arrays.sort(string);

        selectMethod("binary", string, keyword);
        selectMethod("insertion", str.split(" "), "");
        selectMethod("bubble", str.split(" "), "");
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a String.");
        String str = sc.nextLine();
        System.out.println("Enter a keyword to search in the String.");
        String keyword = sc.nextLine();
        sc.close();
        sortAndSearch(str, keyword);
    }

    private static void binarySearch(String[] str, String keyword, int start, int end){
        int mid = (end + start)/2;

        if(start > mid || end < mid){
            System.out.println(keyword+" Does not Exist!");
            return;
        }        
        int result = str[mid].compareTo(keyword);
        
        if(result == 0) {
            System.out.println("Keyword Found! "+keyword);
            return;
        }
        if(result > 0) {
            binarySearch(str, keyword, start, mid - 1);
        }
        else {
            binarySearch(str, keyword, mid + 1, end);
        }
        
    }

    private static void insertionSort(String[] str){
        for(int i=1; i<str.length; i++) {
            for(int j=i; j >= 1; j--) {
                int res = str[j].compareTo(str[j-1]);
                if(res < 0){
                    String temp = str[j];
                    str[j] = str[j-1];
                    str[j-1] = temp;                    
                }
            }
        }
        printArray(str);
    }

    public static void bubbleSort(String[] str){
        for(int i=0;i<str.length;i++){
            for(int j=0; j<str.length-i-1; j++){
                int res = str[j].compareTo(str[j+1]);
                if(res > 0){
                    String temp = str[j];
                    str[j] = str[j+1];
                    str[j+1] = temp;
                }
            }
        }
        printArray(str);
    }

    private static void selectMethod(String type, String[] arr, String keyword){
        switch(type){
            case "binary": binarySearch(arr, keyword.toLowerCase(), 0 , arr.length-1);
                break;
            case "insertion": insertionSort(arr);
                break;
            case "bubble": bubbleSort(arr);
                break;
            default: System.out.println("Nothing executed!!");
        }
    }

    private static void printArray(String[] arr){
        System.out.println();
        for(String str: arr){
            System.out.print(str+" : ");
        }
        System.out.println();
    }
}

// Hey You are such a bad ass and mayank is such a fucking moron.