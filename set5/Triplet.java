package set5;

import java.util.Scanner;
import java.util.Arrays;

public class Triplet {

    public static void findTriplets(int[] arr){
        int length =  arr.length;
        boolean found = false;
        Arrays.sort(arr);
        int low, high;

        for(int i=0; i < length - 2; i++){
            low = i +1;
            high = length -1;

            while(high > low){
                int triplet = arr[i] + arr[low] + arr[high];

                if(triplet == 0) {
                    found = true;
                    System.out.println(arr[i]+": "+arr[low]+": "+arr[high]);
                    low++;  high--;
                }

                if(triplet < 0)
                    low++;
                if(triplet > 0)
                    high--;
            }
        }
        if(!found)
            System.out.println("No Triplet Found!");
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter n");
        int n = sc.nextInt();
        int[] arr = new int[n];
        
        for(int i=0;i<n;i++){
            arr[i] = sc.nextInt();
        }
        sc.close();
        findTriplets(arr);
    }
}