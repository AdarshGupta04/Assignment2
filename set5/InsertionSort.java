package set5;

import java.util.Scanner;

public class InsertionSort {

    public static void insertionSort(String sentence) {
        String[] words = sentence.split(" ");
        printArray(words);

        for(int i=1; i<words.length; i++) {
            for(int j=i; j >= 1; j--) {
                int res = words[j].compareToIgnoreCase(words[j-1]);
                if(res < 0){
                    String temp = words[j];
                    words[j] = words[j-1];
                    words[j-1] = temp;                    
                }
            }
        }
        printArray(words);
    }
    
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the sentence.");
        String sentence = sc.nextLine();
        sc.close();
        insertionSort(sentence);
    }

    private static void printArray(String[] arr){
        System.out.println();
        for(String str: arr){
            System.out.print(str+" : ");
        }
        System.out.println();
    }
}