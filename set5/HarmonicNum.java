package set5;

import java.util.Scanner;

public class HarmonicNum {

    public static double findHarmonicNum(int num){
        if(num == 1)
            return 1;
        return 1.0/num + findHarmonicNum(num - 1);
    }
    

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Enter a number greater than 0");
        int num = sc.nextInt();
        sc.close();

        if(num <= 0){
            System.out.println("You entered an Invalid Number!");
            return;
        }

        double number = findHarmonicNum(num);
        System.out.println("Harmonic number for "+num+" position is: "+number);

    }
}