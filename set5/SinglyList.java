package set5;

import list.Node;
import list.NodeStaticFeatures;

public class SinglyList {

    public static Node insertNodeAtStart(Node head, int value){
        Node newNode = new Node(head, value);
        return newNode;
    }

    public static Node insertNodeAtEnd(Node head, int value){
        Node newNode = new Node(null, value);
        Node tail = getTailNode(head);
        tail.next = newNode;

        return head;
    }

    public static Node insertNode(Node head, int value, int position){
        Node temp = head;
        int count = 0;
        Node newNode = new Node(null, value);

        while(temp.next != null && count != position - 1){
            count++;
            temp = temp.next;
        }

        if(count == 0){
            newNode.next = head;
            return newNode;
        }
        else{
            newNode.next = temp.next;
            temp.next = newNode;
        }
        
        return head;
    }

    public static Node deleteStartNode(Node head){
        return head.next;
    }

    public static Node deleteLastNode(Node head){
        Node temp = head;
        
        while(temp.next.next != null){
            temp = temp.next;
        }
        temp.next = null;
        return head;
    }

    public static Node deleteNode(Node head, int position){
        Node temp = head;
        int count = 0;

        while(temp.next != null && count != position-1) {
            temp = temp.next;
            count++;
        }

        if(count == 0){
            return temp.next;
        }
        
        temp.next = temp.next.next;
        return head;        
    }
    
    public static void main(String[] args) {
        Node head = NodeStaticFeatures.createLL();
        displayList(head);
        NodeStaticFeatures.printList(head); 

        //insertions
        head = insertNodeAtStart(head, 70);
        displayList(head);

        head = insertNodeAtEnd(head, 80);
        displayList(head);
        
        head = insertNode(head, 90, 2);
        displayList(head);

        //deletions
        head = deleteLastNode(head);
        displayList(head);

        head = deleteStartNode(head);
        displayList(head);

        head = deleteNode(head, 3);
        displayList(head);


    }

    private static Node getTailNode(Node head) {
        while(head.next != null){
            head = head.next;
        }
        return head;
    }

    public static void displayList(Node head) {
        while(head != null){
            System.out.print(head.data+" --> ");
            head = head.next;
        }
        System.out.println("End");
    }
}