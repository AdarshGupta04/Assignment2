package set1;

import java.util.Stack;

import list.Node;
import list.NodeStaticFeatures;

public class ReverseLL {

    //Reversing list iteratively.
    public static Node reverseIteratively(Node head) {
        Node current = head;
        Node temp;
        Node prev = null;

        while(current != null){
            temp = current.next;
            current.next = prev;
            prev = current;
            current = temp;
        }

        return prev;
    }

    //Reversing list recursively.
    public static Node reverseList(Node head) {
        if(head == null || head.next == null){
            return head;
        }

        Node newList = reverseList(head.next);
        head.next.next = head;
        head.next = null;

        return newList;
    }
    
    //Reversing using Stack.
    public static Node reverseUsingStack(Node head){
        Stack<Node> stack = new Stack<>();
        Node prev = null;
        Node newHead = null;

        while(head != null){
            stack.push(head);
            head = head.next;
        }

        while(!stack.isEmpty()){
            Node currNode = stack.pop();
            currNode.next = null;

            if(newHead == null){
                newHead = currNode;
                prev = currNode;
                continue;
            }

            prev.next = currNode;
            prev = currNode;

        }
        return newHead;
    }

    public static void main(String[] args) {
        
        System.out.println("Normal List");
        Node head = NodeStaticFeatures.createLL();
        NodeStaticFeatures.printList(head);

        System.out.println("Reversed List Iteratively...");
        head = reverseIteratively(head);
        NodeStaticFeatures.printList(head);

        System.out.println("Reversed List Recursively...");
        head = NodeStaticFeatures.createLL();
        head = reverseList(head);
        NodeStaticFeatures.printList(head);

        System.out.println("Reversed List using Stack...");
        head = NodeStaticFeatures.createLL();
        head = reverseUsingStack(head);
        NodeStaticFeatures.printList(head);

    }
}