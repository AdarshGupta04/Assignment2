package set1;


import java.util.Scanner;

public class ReplaceString {

    public static void replaceAndPrint(String userName){
        if(userName.length() >= 3){
            System.out.println("Hello "+userName+", How are you?");
        }
        System.out.println("UserName is too short, kindly give a proper username atleast 3 characters long!!");
    }

    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        String userName = sc.next();
        sc.close();
        replaceAndPrint(userName);
    }
}