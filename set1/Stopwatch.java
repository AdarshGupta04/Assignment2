package set1;

import java.util.Scanner;

public class Stopwatch {

    private static long getCurrentTime(){
        return System.currentTimeMillis();
    }
    
    private static void stopwatch(Scanner sc){
        System.out.println("Hit any key and then Enter to start the stopwatch...");
        sc.next();
        long start = getCurrentTime();
        System.out.println("Hit any key and then Enter to stop the stopwatch...");
        sc.next();
        long end = getCurrentTime();
        System.out.print("Stopwatch ran for: ");
        System.out.println(end - start+" ms");
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        stopwatch(sc);
        sc.close();
    }
}