package set1.crossgame;

import java.util.Scanner;

public class TicTacToe {

    public static void playGame(Scanner sc){
        String matchResult = "";
        boolean computerTurn = true;
        String user = "c";
        System.out.println("Match result length: "+matchResult.length());
        int boxNumber = 0;

        while(matchResult.length() == 0){
            if(computerTurn){
                boxNumber = computerTurn();
                user = "c";
            }else{
                System.out.println("Enter the block Number.");
                boxNumber = sc.nextInt();
                user = "u";
                if(!isValidBlock(boxNumber)){
                    continue;
                }
            }
            makeMove(boxNumber, user, Board.board);
            Board.unOccupied[boxNumber] = false;
            computerTurn = !computerTurn;
            Board.printBoard();
            matchResult = checkWinner(boxNumber, Board.board);

        }

        System.out.println("Match Result: "+matchResult);
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Board.printBoard();
        playGame(sc);
        sc.close();
    }

    private static String checkWinner(int boxNumber, String[][] board){
        String res1 = "";
        String res2 = "";
        String res3 = "";
        String res4 = "";

        switch(boxNumber){
            case 1: res1 = board[0][0] + board[0][1] + board[0][2];
                    res2 = board[0][0] + board[1][1] + board[2][2];
                    res3 = board[0][0] + board[1][0] + board[2][0];
                break;
            case 2: res1 = board[0][0] + board[0][1] + board[0][2];
                    res2 = board[0][1] + board[1][1] + board[2][1];
                break;
            case 3: res1 = board[0][0] + board[0][1] + board[0][2];
                    res2 = board[2][0] + board[1][1] + board[0][2];
                    res3 = board[0][2] + board[1][2] + board[2][2];
                break;
            case 4: res1 = board[0][0] + board[1][0] + board[2][0];
                    res2 = board[1][0] + board[1][1] + board[1][2];
                break;
            case 5: res1 = board[0][0] + board[1][1] + board[2][2];
                    res2 = board[2][0] + board[1][1] + board[0][2];
                    res3 = board[0][1] + board[1][1] + board[2][1];
                    res3 = board[1][0] + board[1][1] + board[1][2];
                break;
            case 6: res1 = board[0][2] + board[1][2] + board[2][2];
                    res2 = board[1][0] + board[1][1] + board[1][2];
                break;
            case 7: res1 = board[0][0] + board[1][0] + board[2][0];
                    res2 = board[0][2] + board[1][1] + board[2][0];
                    res3 = board[2][0] + board[2][1] + board[2][2];
                break;
            case 8: res1 = board[0][1] + board[1][1] + board[2][1];
                    res2 = board[2][0] + board[2][1] + board[2][2];
                break;
            case 9: res1 = board[0][2] + board[1][2] + board[2][2];
                    res2 = board[0][0] + board[1][1] + board[2][2];
                    res3 = board[2][0] + board[2][1] + board[2][2];
                break;
            
        }

        if(res1.equals("ccc") || res2.equals("ccc") || res3.equals("ccc") || res4.equals("ccc"))
            return "Computer Wins";
        else if(res1.equals("uuu") || res2.equals("uuu") || res3.equals("uuu") || res4.equals("uuu"))
            return "User Wins";
        else{
            if(isDraw())
               return "Game Draw!";
        }
        return "";
    }

    private static void makeMove(int box, String user, String[][] board){        
        switch(box){
            case 1: board[0][0] = user;
                break;
            case 2: board[0][1] = user;
                break;
            case 3: board[0][2] = user;
                break;
            case 4: board[1][0] = user;
                break;
            case 5: board[1][1] = user;
                break;
            case 6: board[1][2] = user;
                break;
            case 7: board[2][0] = user;
                break;
            case 8: board[2][1] = user;
                break;
            case 9: board[2][2] = user;
                break;
        }
    }

    private static int computerTurn(){
        int res;
        do{
            res = random();
        }while(!Board.unOccupied[res]);
        return res;
    }

    private static int random(){
        return (int)(Math.random() * 10);
    }

    private static boolean isDraw(){
        int count = 0;
        for(int i=1;i<Board.unOccupied.length;i++)
            if(!Board.unOccupied[i])
                count++;
        
        if(count == 9)
            return true;
        return false;
    }   

    private static boolean isValidBlock(int block){
        if(block < 1 || block > 9){
            System.out.println("Please Enter a valid block!");
            return false;
        }
        if(!Board.unOccupied[block]){
            System.out.println("This block has already occupied!, Please Enter a different Block.");
            return false;
        }
        return true;
    }
}