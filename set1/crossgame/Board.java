package set1.crossgame;

import java.util.Arrays;

public class Board {
    public static String[][] board = new String[3][3];
    public static boolean[] unOccupied = new boolean[10];
    public static char[] crossMarks = new char[10];
    public static char turn = 'c';

    static {
        Arrays.fill(crossMarks, 'u');
        Arrays.fill(unOccupied, true);
        fillBoard();
    }

    protected static void fillBoard(){
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                board[i][j] = "empty";
            }
        }
    }
    
    protected static void printBoard(){
        int count = 1;
        System.out.println("----------------------");
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                if(count == 1 || count == 4 || count == 7)
                    if(unOccupied[count])
                        System.out.print(" | "+ count +" ");
                    else
                        System.out.print(" | "+getBoardValue(count)+" ");

                if(count == 2 || count == 5 || count == 8)
                    if(unOccupied[count])
                        System.out.print(" | "+ count +" | ");
                    else
                        System.out.print(" | "+getBoardValue(count)+" | ");

                if(count%3 == 0)
                    if(unOccupied[count])
                        System.out.print(" "+ count +" |");
                    else
                        System.out.print(" "+getBoardValue(count)+" |");
                count++;
            }
            System.out.println();
        }
        System.out.println("----------------------");
    }

    protected static String getBoardValue(int boxNumber){
        switch(boxNumber){
            case 1: return board[0][0]; 
            case 2: return board[0][1]; 
            case 3: return board[0][2]; 
            case 4: return board[1][0]; 
            case 5: return board[1][1]; 
            case 6: return board[1][2]; 
            case 7: return board[2][0]; 
            case 8: return board[2][1]; 
            case 9: return board[2][2]; 
        }
        return "";
    }
    
    
}