package set1;

import java.util.Scanner;

public class Palindrome {

    public static boolean checkPalindromePossibility(String word){
        if(word.length() == 1){
            return true;
        }
        int[] frequency = getFrequency(word);
        return checkPalindrome(frequency);        
    }

    public static String getPalindrome(String word){
        String[] words = palindrome(word);

        for(String str: words){
            boolean result = isPalindrome(str);
            if(result){
                return str;
            }
        }
        return "false";
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a String to check if it can be converted into an Palindrome...");
        String word = sc.next();
        sc.close();
        boolean result = checkPalindromePossibility(word);
        System.out.println("result: "+result);
        
        if(result){
            String palindrome = getPalindrome(word);
            System.out.println("Palindrome possible. "+palindrome);
        }else {
            System.out.println("Palindrome not possible.");
        }
    }

    private static String[] palindrome(String word){
        if(word.length() == 1){
            String[] arr = {word};
            return arr;
        }

        int index = 0;
        String[] words = palindrome(word.substring(1));
        int newLength = words.length * (words[0].length() +1);
        String[] allWords = new String[newLength];
        char ch = word.charAt(0);

        for(String currWord: words){
            for(int i=0; i<currWord.length(); i++){
                allWords[index++] = currWord.substring(0, i) + ch + currWord.substring(i);
            }
            allWords[index++] = currWord + ch;
        }
        return allWords;
    }

    private static boolean isPalindrome(String word){
        if(word.length() == 1 || word.length() == 0){
            return true;
        }

        if(word.charAt(0) == word.charAt(word.length()-1)){
            return isPalindrome(word.substring(1, word.length()-1));
        }

        return false;
    }

    //utility methods related to possiblility of palindrome.
    private static int[] getFrequency(String word){
        int[] frequency = new int[26];

        for(int i=0; i<word.length(); i++){
            frequency[word.charAt(i) - 'a']++;
        }

        return frequency;
    }

    private static boolean checkPalindrome(int[] frequency){
        boolean oddFlag = false;

        for(int i=0; i<frequency.length; i++) {
                if(frequency[i]%2 != 0){
                    if(oddFlag){
                        return false;
                    }
                    oddFlag = true;
                }
        }
        return true;
    }
}