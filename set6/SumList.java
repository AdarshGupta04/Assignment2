package set6;

import java.util.List;
import java.util.ArrayList;

import list.Node;
import list.NodeStaticFeatures;

public class SumList {
    private static final int BASE = 10;

    private static Node createList(int[] arr){
        Node head = null;
        Node tail = null;

        for(int i=0;i<arr.length;i++){
            Node newNode = new Node(null, arr[i]);
            if(head == null) {
                head = newNode;
                tail = head;
                continue;
            }
            tail.next = newNode;
            tail = tail.next;
        }
        return head;
    }

    private static int getNumber(Node head) {
        if(head == null)
            return 0;

        int sum = 0;
        int power = 0;
        do{
            sum += head.data * (int) Math.pow(BASE, power);
            head = head.next;
            power++;
        }while(head != null);

        return sum;
    }

    private static int[] numToArray(int num) {
        List<Integer> list = new ArrayList<>();

        while(num > 0) {
            int temp = num%10;
            list.add(temp);
            num = num/10;
        }
        int[] arr = new int[list.size()];
        for(int i=0;i<list.size();i++)
            arr[i] = list.get(i);

        return arr;
    }

    public static void main(String[] args) {
        Node list1 = createList(new int[]{2, 4, 3});
        Node list2 = createList(new int[]{5, 6, 4});

        NodeStaticFeatures.printList(list1);
        NodeStaticFeatures.printList(list2);

        int num1 = getNumber(list1);
        int num2 = getNumber(list2);

        System.out.println("num1: "+num1);
        System.out.println("num2: "+num2);
        
        int res = num1 + num2;
        System.out.println("res: "+res);

        int[] arr = numToArray(res);
        Node list = createList(arr);

        NodeStaticFeatures.printList(list);
    }
}