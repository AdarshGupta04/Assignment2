package set6;

import java.util.Arrays;

public class NextPermutation {

    public static void nextPermutation(int[] num){
        if(num == null || num.length < 0)
            return;

        int i = num.length - 2;
        while(i >= 0 && num[i] >= num[i+1]) 
            i--;
        if(i >= 0){
            int j = num.length - 1;
            while(num[j] <= num[i])
                j--;
            swap(num, i, j);
        }
        reverse(num, i+1);
    }

    private static void swap(int[] arr, int i, int j){
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    private static void reverse(int[] arr, int i) {
        int len = arr.length - 1;
        while(i < len)
            swap(arr, i++, len--);
    }
    
    public static void main(String[] args) {
        int[] arr = new int[]{1, 2, 3};
        printArray(arr);
        nextPermutation(arr);
        System.out.println(" - ");
        printArray(arr);
    }

    private static void printArray(int[] arr) {
        Arrays.stream(arr).forEach(System.out::print);
    }
}