package set6;

import java.util.HashSet;
import java.util.Set;

public class LongestSubstring {

    private static String getLongestSubstring(String str) {
        if(str.length() == 0)
            return "";
        
        String str1 =   getLongestSubstring(str.substring(1)) + str.charAt(0);
        String str2 = getLongestSubstring(str.substring(1));

        if(isValid(str1) && isValid(str2)){
            if(str1.length() > str2.length())
                return str1;
            else
                return str2;
        }
        
        if(isValid(str1))
            return str1;
        
        if(isValid(str2))
            return str2;      

        return "";

    }

    private static boolean isValid(String str){
        Set<Character> set = new HashSet<>();

        for(int i=0;i<str.length(); i++) {
            char ch = str.charAt(i);

            if(set.contains(ch))
                return false;
            set.add(ch);
        }
        return true;
    }
    

    public static void main(String[] args) {
        String res = getLongestSubstring("abcabcbb");
        System.out.println("res: "+res);
    }
}