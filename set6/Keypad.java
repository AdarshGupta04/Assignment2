package set6;

public class Keypad {
    
    private static String[] getCombinations(int num) {
        if(num < 10){
            return getChars(num);
        }
         
        int n = num % 10;
        String[] smallAns = getCombinations(num/10);
        String[] chars = getChars(n);
        int newLen = smallAns.length * chars.length;

        String[] ans = new String[newLen];
        int k = 0;
        
        for(int i=0;i<smallAns.length; i++){
            for(int j=0;j<chars.length;j++){
                ans[k] = smallAns[i] + chars[j];
                k++;
            }
        }
        return ans;
    }
    
    public static void main(String[] args) {
        int num = 234;
        String[] res = getCombinations(num);
        printArray(res);
    }

    private static String[] getChars(int num){
        String[] res;
        switch(num){
            case 2: res = new String[]{"a", "b", "c"};
                break;
            case 3: res = new String[]{"d", "e", "f"};
                break;
            case 4: res = new String[]{"g", "h", "i"};
                break;
            case 5: res = new String[]{"j", "k", "l"};
                break;
            case 6: res = new String[]{"m", "n", "o"};
                break;
            case 7: res = new String[]{"p", "q", "r", "s"};
                break;
            case 8: res = new String[]{"t", "u", "v"};
                break;
            case 9: res = new String[]{"w", "x", "y", "z"};
                break;
            default: res = new String[1];
                break;
        }
        return res;
    }

    private static void printArray(String[] arr){
        System.out.println();
        for(String str: arr)
            System.out.print(str+", ");
        System.out.println();
    }
}