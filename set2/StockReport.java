package set2;

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class StockReport {

    public static void printStockReport(StockPortfolio stockPortfolio){
        for(Stock currStock: stockPortfolio.stocks){
            System.out.println(currStock.name+": "+currStock.getStockValue());
        }

        System.out.println("Total Value of all Stocks: "+stockPortfolio.getTotalStocksValue());
    }
    

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter number of stocks you have...");
        int noOfStocks = sc.nextInt();
        List<Stock> stocks = new ArrayList<>();

        for(int i=0; i<noOfStocks; i++){
            sc.nextLine();
            Stock stock = new Stock();
            System.out.println("Enter the stock name");
            stock.name = sc.nextLine();
            System.out.println("Enter the number of "+stock.name+" stock");
            stock.noOfShare = sc.nextInt();
            System.out.println("Enter the stock price");
            stock.price = sc.nextInt();

            stocks.add(stock);
        }
        sc.close();

        StockPortfolio stockPortfolio = new StockPortfolio(stocks);
        printStockReport(stockPortfolio);
    }
}