package set2;

import java.util.Scanner;

public class Anagram {

    public static boolean isAnagram(String word1, String word2){
        if(word1.length() != word2.length()){
            return false;
        }
        int xor = 0;

        for(int i=0;i<word1.length();i++){
            xor = xor ^ word1.charAt(i);
            xor = xor ^ word2.charAt(i);
        }

        if(xor == 0){
            return true;
        }
        return false;
    }
    

    public static void main(String[] args) {
        java.util.Scanner sc = new Scanner(System.in);
        System.out.println("Enter first word.");
        String word1 = sc.next();
        System.out.println("Enter second word to check for Anagram.");
        String word2 = sc.next();
        sc.close();

        boolean result = isAnagram(word1, word2);

        if(result){
            System.out.println("Anagrams");
        }else{
            System.out.println("Not Anagrams");
        }

    }
}