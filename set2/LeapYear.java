package set2;

import java.util.Scanner;

public class LeapYear {

    public static boolean isLeapYear(int year){
        boolean validFlag = isValidYear(year);
        if(!validFlag){
            return false;
        }

        if(year%400 == 0 || year%100 != 0 && year%4 == 0){
            return true;
        }
        return false;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter a year.");
        int year = sc.nextInt();
        sc.close();

        boolean result = isLeapYear(year);

        if(result){
            System.out.println("Leap Year");
        }else{
            System.out.println("Not a Leap Year");
        }
    }

    private static boolean isValidYear(int year){
        int digits = noOfDigits(year);
        if(digits == 4){
            return true;
        }
        System.out.println("Not a valid year!!");
        return false;
    }

    private static int noOfDigits(int year){
        if(year < 10){
            return 1;
        }
        return noOfDigits(year/10) + 1;
    }
}