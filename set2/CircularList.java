package set2;

import java.util.Scanner;

import list.Node;
import list.NodeStaticFeatures;

public class CircularList {

    public static Node addElement(Node head, int element, int index){
        if(index == 1){
            Node newNode = new Node(head.next, element);
            head.next = newNode;
            return newNode;
        }
        if(index == 0){
            Node newNode = new Node(head, element);
            Node tail = NodeStaticFeatures.getCircularListTail(head);
            tail.next = newNode;
            return newNode;
        }

        addElement(head.next, element, index-1);
        return head;     
    }    

    public static Node deleteElement(Node head, int element) {
        Node temp = head;

        do{
            if(temp.data == element){
                Node tail = NodeStaticFeatures.getCircularListTail(head);
                tail.next = temp.next;
                System.out.println("Deleted Successfully!");
                return temp.next;
            }

            if(temp.next != null && temp.next.data == element){
                temp.next = temp.next.next;
                System.out.println("Deleted Successfully.");
                return head;
            }

            temp = temp.next;
        }while(temp.next != head);

        System.out.println("Element Does not exist!");
        return head;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Node head = NodeStaticFeatures.createCircularLL();
        System.out.println("This is the list.");
        NodeStaticFeatures.printCircularList(head);

        System.out.println("Enter a position (index) to enter element at.");
        int position = sc.nextInt();
        System.out.println("Enter a element to insert at position (index) "+position);
        int newElement = sc.nextInt();

        int length = NodeStaticFeatures.circularListLength(head);
        if(position > length){
            sc.close();
            System.out.println("position can not be greater than the length of the list.");
            return;
        }

        head = addElement(head, newElement, position);
        NodeStaticFeatures.printCircularList(head);

        System.out.println("Enter an integer element you want to delete.");
        int element = sc.nextInt();
        sc.close();

        head = deleteElement(head, element);
        NodeStaticFeatures.printCircularList(head);
    }
}