package set2;

import java.util.List;

public class StockPortfolio {
    
    public List<Stock> stocks;

    
    StockPortfolio(){
        
    }

    StockPortfolio(List<Stock> stocks){
        this.stocks = stocks;
    }

    public int getTotalStocksValue(){
        int totalValue = 0;

        for(Stock stock: stocks) {
            totalValue += stock.getStockValue();
        }
        return totalValue;
    }

}