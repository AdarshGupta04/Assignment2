package set2;

public class Stock {
    public String name;
    public int noOfShare;
    public int price;

    Stock(){

    }

    public Stock(String name, int noOfShare, int price){
        this.name = name;
        this.noOfShare = noOfShare;
        this.price = price;
    }

    public int getStockValue(){
        return noOfShare*price;
    }
}