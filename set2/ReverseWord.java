package set2;

import java.util.Scanner;

public class ReverseWord {
    
    public static String reverseTheWords(String sentence){
        
        String[] splitted = sentence.split(" ");
        String newSentence = "";

        for(String word: splitted){
            newSentence += reverseTheWord(word) + " ";
        }
        return newSentence;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the sentence.");
        String sentence = sc.nextLine();
        sc.close();
        String reversed = reverseTheWords(sentence);
        System.out.println("Reversed: "+reversed);
    }

    private static String reverseTheWord(String word){
        if(word.length() == 1){
            return word;
        }
        return reverseTheWord(word.substring(1)) + word.charAt(0);
    }
}